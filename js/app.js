// Теоретичні питання:

// 1. setTimeout() викликає функцію один раз через заданий час і все, а
//    setInterval() постійно з інтервалом і до відміни функції;
// 2. Якщо в функцію setTimeout() передати нульову затримку то вона виконається без затримки, але тільки
//    після виконання всього коду, тому спрацює вона не миттєво;
// 3. Важливо не забувати викликати функцію clearInterval(), коли цикл запуску вже не потрібен, 
//    тому що функція setInterval() продовжить працювати далі та нагружати ресурси.

// Завдання:

const images = document.querySelectorAll('.image-to-show');
const btn = document.querySelector('.btn-wrapper');
const stopBtn = document.querySelector('.stopBtn');
const resumeBtn = document.querySelector('.resumeBtn');
let i = 0;

function setDefaultImage() {
  resumeBtn.disabled = true;
  images[i].style.display = 'block';
  i++;
}

function changeImage() {
  if (i > 3) {
    i = 0;
  }
  let activeImage = images[i];
  images.forEach((img) => {
    img.style.display = 'none';
  });
  activeImage.style.display = 'block';
  i++;
}

setDefaultImage();
let timer = setInterval(changeImage, 3000);

function checkEvent(ev) {
  ev.preventDefault();
  ev.target.disabled = true;
  if (ev.target === stopBtn) {
    resumeBtn.disabled = false;
    clearInterval(timer);
  } else if (ev.target === resumeBtn) {
    stopBtn.disabled = false;
    timer = setInterval(changeImage, 3000);
  }
}

btn.addEventListener('click', checkEvent);
